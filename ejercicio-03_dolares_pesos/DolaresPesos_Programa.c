/* 
	Autor: Brito Segura Angel
	Fecha: 14/08/2020
	Descripcion: Programa que convierte de dolares (USD) a pesos (MXN)
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>
#include <ctype.h> // Para manejo de cadenas

// Declaracion de constantes
#define dollar 21.76 // Equivalencia de un dolar en pesos

// Funcion que convierte a mayusculas una palabra ingresada
char convierteMayusculas () {
	// Declaracion de variables locales
	char palabra;
	
	printf("\nDesea repetir el programa (SI/NO): ");
	fflush(stdin); // Limpieza de la variable de entrada para evitar errores
	scanf("%s", &palabra);
	palabra = toupper(palabra); // Convierte todo lo ingresado a mayusculas
	
	return (palabra); // Regresar el valor de la palabra ingresada
}

// Funci�n principal
void main(){
	// Declaracion de variables locales
	char repetir;
	float dolares, pesos;
	
	do {
		// Inicializar las variables reales
		dolares = 0.0;
		pesos = 0.0;
		
		printf("\nIntroduce los dolares (USD) a convertir: $");
		scanf("%f", &dolares);
		
		pesos = dollar * dolares;
		
		printf("Los %.2f dolares (USD) equivalen a: $%.2f (MXN)\n", dolares, pesos);
		
		repetir = convierteMayusculas();
	} while(repetir == 'S'); // Mientras ingrese la palabra "SI" repetira el programa
}
