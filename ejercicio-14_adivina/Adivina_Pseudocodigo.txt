Función random(desde, hasta)
Inicio
    Variable aleatorio es Numérico Entero
    aleatorio = desde + aleatorio() / (rango_máximo_aleatorio() / (hasta - desde + 1) + 1)
    Devolver aleatorio
Fin

Inicio
    Variable intento es Numérico Entero
    Variable númeroAleatorio es Numérico Entero
    Variable número es Numérico Entero
    Variable victoria es Cadena de caracteres
    Variable derrota es Cadena de caracteres

    número = 0
    victoria = "GANASTE"
    derrota = "PERDISTE"

    Escribir "ADIVINA UN NUMERO"
    Escribir "¡Solo tienes 5 intentos!"

    númeroAleatorio = random(1, 100)
    intento = 1

    while (intento <= 5)
        Escribir "Intento ", intento
        Leer "Introduce un número entre 1 y 100: ", numero
        Si (número >= 1 AND número <= 100) entonces
            Si (númeroAleatorio == número) entonces
                Escribir " El número aleatorio generado era ", victoria, númeroAleatorio
                romperCiclo;
            sino
                Escribir "El no es el número correcto, vulelve a intentarlo", número
            fin-si
        sino
            Escribir "Deber ser un número entre 1 y 100"
        fin-si
        intento = intento + 1
    end-while

    Si (númeroAleatorio != número) entonces
        Escribir " El número correcto era ", derrota, númeroAleatorio
    fin-si
Fin