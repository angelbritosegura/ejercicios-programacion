/* 
	Autor: Brito Segura Angel
	Fecha: 22/08/2020
	Descripcion: Programa que pregunta al usuario que adivine un numero
*/

// Bibliotecas
#include <stdio.h> // Para funciones de entrada y salida de datos
#include <stdlib.h> // Para la funcione rand, asi como conocer su valor maximo RAND_MAX
#include <unistd.h> // Funciones estandar de UNIX (Linux)

// Declaracion de constantes
#define intentos 5
#define limiteInferior 1
#define limiteSuperior 100

// Funcion que devuelve un numero aleatorio entre un valor minimo y maximo, incluyendo los extremos
int random(int desde, int hasta){
	int aleatorio;
	// La constante RAND_MAX nos da el valor maximo que llega la funcion rand(), esta funcion nos devuleve numeros enteros aleatorios
	aleatorio = desde + rand() / (RAND_MAX / (hasta - desde + 1) + 1);
	return aleatorio;
}

// Funcion principal
void main(){
	// Declaraci�n de variables
	int intento, numeroAleatorio, numero;
	char victoria[]="GANASTE", derrota[]="PERDISTE";
	
	srand(getpid()); // Alimentar a la funcion rand con un valor no fijo (en este caso, el id del proceso del programa cambiara siempre) para generar n�meros aleatorios
	
	numeroAleatorio = random(limiteInferior, limiteSuperior); // Llamar a la funcion generadora de numeros aleatorios
	intento = 1;
	
	puts("\t\t\tADIVINA UN NUMERO");
	printf("�Solo tienes %d intentos!\n", intentos);
	
	while (intento <= intentos) {
		printf("\nIntento %d\n", intento);
		printf("Introduce un numero entre %d y %d: ", limiteInferior, limiteSuperior);
		scanf("%d", &numero);
		if (numero >= limiteInferior && numero <= limiteSuperior) {
			if (numeroAleatorio == numero) {
				printf("\n\t%s\nEl numero aleatorio generado era %d", victoria, numeroAleatorio);
				break;
			} else {
				printf("El %d no es el numero correcto, vuelve a intentarlo\n", numero);
			}
		} else {
			printf("\nDebe ser un numero entre %d y %d\n", limiteInferior, limiteSuperior);
		}
		intento++;
	}
	if (numeroAleatorio != numero)
		printf("\n\t%s\nEl numero correcto era %d", derrota, numeroAleatorio);
}
