/* 
	Autor: Brito Segura Angel
	Fecha: 22/08/2020
	Descripcion: Programa que imprime los numeros del 1 al 100, aplicando ciertas reglas
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>

// Declaracion de constantes
#define limiteInferior 1
#define limiteSuperior 100

// Funcion principal
void main(){
	int i; // Declaracion del contador i
	
	printf("Numeros del %d al %d (aplicando las reglas ping-pong): ", limiteInferior, limiteSuperior);
	
	// Ciclo para obtener los numeros del 1 al 100
	for(i = limiteInferior; i <= limiteSuperior; i++) {
		// Si es divisible entre 3
		if (i%3 == 0) {
			// Y divisible entre 5
			if (i%5 == 0) {
				printf("\nping-pong");
			} else {
				printf("\nping");
			}
		} else if (i%5 == 0) {
			printf("\npong");
		} else {
			// En caso de que no sea divisible entre 3 y/o 5, imprimir� el n�mero correspondiente
			printf("\n%d", i);
		}
	}
}
