/* 
	Autor: Brito Segura Angel
	Fecha: 15/08/2020
	Descripcion: Programa que calcula el perimetro de un triangulo escaleno
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>

// Funcion que convierte a mayusculas una palabra ingresada
char convierteMayusculas () {
	// Declaracion de variables locales
	char palabra;
	
	printf("\nDesea obtener otro perimetro (SI/NO): ");
	fflush(stdin); // Limpieza de la variable de entrada para evitar errores
	scanf("%s", &palabra);
	palabra = toupper(palabra); // Convierte todo lo ingresado a mayusculas
	
	return (palabra); // Regresar el valor de la palabra ingresada
}

// Funcion principal
void main(){
	// Declaracion de variables locales
	char repetir;
	float lado1, lado2, lado3, perimetro;
	
	do {
		printf("\nIntroduce un lado del triangulo escaleno: ");
		scanf("%f", &lado1);
		printf("Introduce otro lado del triangulo escaleno: ");
		scanf("%f", &lado2);
		printf("Introduce el ultimo lado del triangulo escaleno: ");
		scanf("%f", &lado3);
		
		perimetro = lado1 + lado2 + lado3;
		
		printf("\nEl perimetro del triangulo escaleno es: %.1f\n", perimetro);
		
		repetir = convierteMayusculas();
	} while(repetir == 'S'); // Mientras ingrese la palabra "SI" repetira el programa
}
