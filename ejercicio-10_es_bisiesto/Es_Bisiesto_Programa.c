/* 
	Autor: Brito Segura Angel
	Fecha: 16/08/2020
	Descripcion: Funcion que indica si un a�o es bisiesto o no
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>

// Funcion para establecer si es bisiesto un a�o o no (en el formato "aaaa")
int esBisiesto(int anio) {
	// Si es divisible entre 4 es a�o bisiesto
	if (anio%4 == 0) {
		// Si es divisible entre 100 no es a�o bisiesto
		if (anio%100 == 0) {
			//Excepto si es divisible entre 400 
			if (anio%400 == 0) {
				return 1; // Verdadero
			} else {
				return 0; // Falso
			}
		} else {
			return 1; // Verdadero
		}
	} else {
		return 0; // Falso
	}
}	
