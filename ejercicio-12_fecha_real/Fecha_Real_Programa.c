/* 
	Autor: Brito Segura Angel
	Fecha: 17/08/2020
	Descripcion: Funcion que valida si una fecha es real o no
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>
#include <stdlib.h> // Para manejo de memoria din�mica
#include <ctype.h> // Manejo de caracteres
#include <string.h> // Para manejo de cadenas

// Validar que el mes sea correcto
int validarMes(char *mes) {
	// Variable global
	const char *meses[] = {"enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"};

	// Declaracion de variable
	int i;
	
	// Buscar a que mes correponde
	for (i=0; i<12; i++) {
		if (strcmp(mes, meses[i]) == 0)
			return i; // Valida
	}
	return -1; // Invalida
}

// Funcion para establecer si cumple con la desigualdad
int esBisiesto(int anio) {
	// Si es divisible entre 4 es a�o bisiesto
	if (anio%4 == 0) {
		// Si es divisible entre 100 no es a�o bisiesto
		if (anio%100 == 0) {
			//Excepto si es divisible entre 400 
			if (anio%400 == 0) {
				return 1; // Verdadero
			} else {
				return 0; // Falso
			}
		} else {
			return 1; // Verdadero
		}
	} else {
		return 0; // Falso
	}
}

// Funcion para validar que una fecha es real
int fechaReal(int dia, char *mes, int anio) {
	// Variable local
	int numeroMes;
	
	numeroMes = validarMes(mes); // Validar si el mes es correcto
	
	if (anio >= 0) {
		if (numeroMes != -1) {
			if (numeroMes == 1) {
				// Validar si es bisiesto el a�o
				if (esBisiesto(anio)) {
					// Validar el dia
					if (dia > 0 && dia < 30) {
						return 1; // Valida
					} else {
						return 0; // Invalida
					}
				} else {
					// Validar dia correcto
					if (dia > 0 && dia < 29) {
						return 1; // Valida
					} else {
						return 0; // Invalida
					}
				}
			} else if ((dia > 0 && dia < 31) && (numeroMes == 3 || numeroMes == 5 || numeroMes == 8 || numeroMes == 10)) {
				return 1; // Valida
			} else if ((dia > 0 && dia < 32) && (numeroMes == 0 || numeroMes == 2 || numeroMes == 4 || numeroMes == 6 || numeroMes == 7 || numeroMes == 9 || numeroMes == 11)) {
				return 1; // Valida
			} else {
				return 0; // Invalida
			}
		} else {
			return 0; // Invalida
		}
	} else {
		return 0; // Invalida
	}
}
