/* 
	Autor: Brito Segura Angel
	Fecha: 15/08/2020
	Descripcion: Programa que imprima la tabla de multiplicar del 2 al 10 de un numero de entrada
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>

// Declaracion de constantes
#define limiteInferior 2
#define limiteSuperior 10

// Funcion principal
void main(){
	// Declaracion de variables
	int numero, i;
	
	// Obtencion del numero de entrada
	printf("\nIntroduce un numero: ");
	scanf("%d", &numero);
	
	printf("\n\tTabla de multiplicar del %d:", numero);
	
	// Ciclo para tener los numeros del 0 al 100
	for(i = limiteInferior; i <= limiteSuperior; i++) {
		printf("\n\t\t%d X %d = %d", numero, i, numero*i);
	}

}
