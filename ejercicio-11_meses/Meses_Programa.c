/* 
	Autor: Brito Segura Angel
	Fecha: 16/08/2020
	Descripcion: Programa que devuelve el mes correspondiente segun un numero entero
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>
#include <stdlib.h> // Para manejo de memoria din�mica
#include <string.h> // Para manejo de cadenas

// Funcion para obtener el mes correspondiente
char *obtenerMes(int n) {
	// Declaracion de constante
	const char *meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
	
	// Declaracion de variables locales
	char *palabra;
	int tamano;
	
	tamano = strlen(meses[n]); // Obtener el tama�o de la palabra correspondiente al mes
	palabra = malloc(tamano); // Reservar en memoria ese tama�o
	
	strcpy(palabra, meses[n]); // Copiar la palabra para retornar el mes
	
	return palabra;
}

// Funcion principal
void main() {
	// Declaracion de variables
	char *mes, mensaje_error[] = {"El numero a introducir debe de estar entre 0 y 11"};
	int numero;
	
	// Lectura de la entrada de datos
	printf("Introduce un numero entre 0 y 11: ");
	scanf("%d", &numero);
	
	if (numero >= 0 && numero < 12) {
		mes = obtenerMes(numero); // Llamamos a la funcion para obtener el mes en palabra
		printf("\nEl mes %d corresponde a %s", numero, mes);
		free(mes); // Liberar el espacio reservado
	} else {
		printf("\n%s\n", mensaje_error);
	}
}
