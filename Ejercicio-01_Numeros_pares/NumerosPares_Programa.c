/* 
	Autor: Brito Segura Angel
	Fecha: 12/08/2020
	Descripcion: Programa que imprime los numeros pares del 0 al 100.
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>

// Declaracion de constantes
#define limiteInferior 0
#define limiteSuperior 100

// Funcion principal
void main(){
	int i; // Declaracion del contador i
	
	printf("Numeros pares del %d al %d: ", limiteInferior, limiteSuperior);
	
	// Ciclo para tener los numeros del 0 al 100
	for(i = limiteInferior; i <= limiteSuperior; i++) {
		// Si el residuo de la division entre 2 es 0
		if (i%2 == 0) {
			// Entonces es un numero par que se tiene que imprimir
			printf("%d ", i);
		}
	}
}
