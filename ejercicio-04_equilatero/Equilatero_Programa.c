/* 
	Autor: Brito Segura Angel
	Fecha: 14/08/2020
	Descripcion: Programa que calcula el perimetro de un triangulo equilatero
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>

// Funci�n que convierte a may�sculas una palabra ingresada
char convierteMayusculas () {
	// Declaraci�n de variables locales
	char palabra;
	
	printf("\nDesea obtener otro perimetro (SI/NO): ");
	fflush(stdin); // Limpieza de la variable de entrada para evitar errores
	scanf("%s", &palabra);
	palabra = toupper(palabra); // Convierte todo lo ingresado a may�sculas
	
	return (palabra); // Regresar el valor de la palabra ingresada
}

// Funcion principal
void main(){
	// Declaraci�n de variables locales
	char repetir;
	float lado, perimetro;
	
	do {
		printf("\nIntroduce un lado del triangulo equilatero: ");
		scanf("%f", &lado);
		
		perimetro = 3*lado;
		
		printf("El perimetro del triangulo equilatero es: %.1f\n", perimetro);
		
		repetir = convierteMayusculas();
	} while(repetir == 'S'); // Mientras ingrese la palabra "SI" repetir� el programa
}
