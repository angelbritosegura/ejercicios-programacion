/* 
	Autor: Brito Segura Angel
	Fecha: 13/08/2020
	Descripcion: Programa que recibe un numero entre 1 y 50 y devuelve la suma de los n�meros consecutivos del 1 hasta ese numero.
*/

// Bibliotecas
#include <stdio.h> // Estandar para entrada y salida de datos
#include <ctype.h> // Para manejo de cadenas

// Declaracion de variables
#define limiteInferior 1
#define limiteSuperior 50

// Funcion que convierte a mayusculas una palabra ingresada
char convierteMayusculas () {
	// Declaracion de variables locales
	char palabra;
	
	printf("\nDesea repetir el programa (SI/NO): ");
	fflush(stdin); // Limpieza de la variable de entrada para evitar errores
	scanf("%s",&palabra);
	palabra = toupper(palabra); // Convierte todo lo ingresado a mayusculas
	
	return (palabra); // Regresar el valor de la palabra ingresada
}

// Funcion principal
void main() {
	// Declaracion de variables locales
	char repetir;
	int sumaConsecutiva, numero, i;
	
	do {
		// Inicializamos las variables
		sumaConsecutiva = 0; 
		numero = 0;
		printf("\nIngresa un numero entre %d y %d: ", limiteInferior, limiteSuperior);
		scanf("%d", &numero);
		
		// Si el numero ingresado esta en el rango de 1  50
		if (numero >= limiteInferior && numero <= limiteSuperior) {
			// Realizara la suma consecutiva de numeros
			for (i = limiteInferior; i <= numero; i++) {
				sumaConsecutiva += i;
			}
			// Imprimira un mensaje con el resultado de la suma
			printf("La suma consecutiva desde %d hasta %d es: %d\n", limiteInferior, numero, sumaConsecutiva);
		} else {
			// En caso contrario, mostrar� un mensaje de error
			printf("\nEl numero ingresado debe de estar entre %d y %d\n", limiteInferior, limiteSuperior);
		}
		repetir = convierteMayusculas();
	} while(repetir == 'S'); // Mientras ingrese la palabra "SI" repetira el programa
}
