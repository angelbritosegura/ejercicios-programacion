/* 
	Autor: Brito Segura Angel
	Fecha: 15/08/2020
	Descripcion: Programa que comprueba el teorema de la desigualdad de un triangulo
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>

// Funcion para establecer si cumple con la desigualdad
int cumpleDesigualdad(float a, float b, float c) {
	if ((a + b) > c) {
		return 1; // Verdadero
	} else {
		return 0; // Falso
	}
}

// Funcion principal
void main(){
	// Declaracion de variables locales
	float segmento1, segmento2, segmento3;
	char mensaje_error[] = "No es posible formar un triangulo con esos segmentos";
	
	// Lectura de los datos de entrada
	printf("\nIntroduce la longitud del segmento 1: ");
	scanf("%f", &segmento1);
	printf("Introduce la longitud del segmento 2: ");
	scanf("%f", &segmento2);
	printf("Introduce la longitud del segmento 3: ");
	scanf("%f", &segmento3);

	// Teorema de la desigualdad de un triangulo
	if (cumpleDesigualdad(segmento1, segmento2, segmento3)) {
		if (cumpleDesigualdad(segmento1, segmento3, segmento2)) {
			if (cumpleDesigualdad(segmento2, segmento3, segmento1)) {
				printf("\nEs posible formar un triangulo con los segmentos introducidos");
			} else {
				printf("\n%s", mensaje_error);
			}
		} else {
			printf("\n%s", mensaje_error);
		}
	} else {
		printf("\n%s", mensaje_error);
	}
		
}
