/* 
	Autor: Brito Segura Angel
	Fecha: 14/08/2020
	Descripci�n: Convertir dos pares de AR -n�meros ar�bigos (A) y romanos (R)- a un solo n�mero entero
*/

// Bibliotecas
#include <stdio.h> // Est�ndar para entrada y salida de datos
#include <ctype.h> // Para manejo de cadenas
#include <conio.h> // Funci�n getch();
#define o 162 // Para � en ASCII
#define u 163 // Para � en ASCII

// Declaraci�n de constantes c�digo ASCII
#define cero 48
#define nueve 57

// Funci�n para detectar si es un d�gito un caracter
int esDigito(char letra) {
	// Declaraci�n local de varible
	int digito;
	// Si la cadena (con su c�digo en ASCII) es un d�gito
	if (letra >= cero && letra <= nueve) {
		digito = letra - cero; // Restar '0' para convertir el caracter a entero
		return digito; // Devolver el d�gito correspondiente
	} else {
		return -1;
	}
}

// Funci�n para convertir un n�mero romano a decimal
int aDecimal(char letra) {
	// Declaraci�n local de varible
	int decimal;
	
	// Dependiendo del n�mero romano ser� la asignaci�n del valor
	switch(letra) {
		case 'I':
			decimal = 1;
			break;
		case 'V':
			decimal = 5;
			break;
		case 'X':
			decimal = 10;
			break;
		case 'L':
			decimal = 50;
			break;
		case 'C':
			decimal = 100;
			break;
		case 'D':
			decimal = 500;
			break;
		case 'M':
			decimal = 1000;
			break;
		default:
			decimal = -1;
			break;
	}
	
	return decimal;
}

// Funci�n principal
void main() {
	// Declaraci�n de variables locales
	char cadena[3];
	int numero[3], calculoPar;
	
	printf("\nIntroduce la cadena: ");
	scanf("%s", &cadena);
	
	// Convertir de caracter a entero
	numero[0] = esDigito(cadena[0]);
	numero[2] = esDigito(cadena[2]);
	
	// Convertir de romano a decimal
	numero[1] = aDecimal(toupper(cadena[1]));
	numero[3] = aDecimal(toupper(cadena[3]));
	
	// Si los n�meros ingresados no son d�gitos
	if (numero[0] == -1 || numero[2] == -1) {
		// Imprimir mensaje de error
		printf("\nNo es un n%cmero decimal la primera o tercera letra ingresada", u);
	} else if (numero[1] == -1 || numero[3] == -1) {
		// Si las letras introducidas no son n�meros romanos, imprimir� un mensaje de error
		printf("\nNo es un n%cmero romano la segunda o cuarta letra ingresada", u);
	} else {
		// Se suman los valores de los pares si R del primer par es mayor o igual al R del segundo par
		if (numero[1] >= numero[3]) {
			calculoPar = numero[0]*numero[1] + numero[2]*numero[3];
			printf("\nConversi%cn: \t %s = %d", o, cadena, calculoPar);
		} else {
			// En caso contrario, se restan
			calculoPar = numero[2]*numero[3] - numero[0]*numero[1];
			printf("\nConversi%cn: \t %s = %d", o, cadena, calculoPar);
		}
	}
	
	getch(); // Detiene la ejecuci�n
}
