/* 
	Autor: Brito Segura Angel
	Fecha: 14/08/2020
	Descripcion: Programa que calcula el perimetro de un triangulo isosceles
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>

// Funcion principal
void main(){
	// Declaracion de variables locales
	float ladoRepetido, lado, perimetro;
	
	// Lectura de los datos de entrada
	printf("Introduce el lado del triangulo isosceles que es igual a otro lado: ");
	scanf("%f", &ladoRepetido);
	printf("Introduce el otro lado faltante del triangulo isosceles: ");
	scanf("%f", &lado);
	
	perimetro = 2*ladoRepetido + lado;

	printf("\nEl perimetro del triangulo isosceles es: %.1f", perimetro);

}
