/* 
	Autor: Brito Segura Angel
	Fecha: 22/08/2020
	Descripcion: Programa que pregunta al usuario que adivine un numero
*/

// Bibliotecas
#include <stdio.h> // Para funciones de entrada y salida de datos
#include <stdlib.h> // Para la funcione rand, asi como conocer su valor maximo RAND_MAX
#include <unistd.h> // Funciones estandar de UNIX (Linux)

// Declaracion de constantes
#define rondas 3
#define apuestaMinima 20
#define apuestaMaxima 100

// Funcion que devuelve un numero aleatorio entre un valor minimo y maximo, incluyendo los extremos
int random(int desde, int hasta){
	int aleatorio;
	// La constante RAND_MAX nos da el valor maximo que llega la funcion rand(), esta funcion nos devuleve numeros enteros aleatorios
	aleatorio = desde + rand() / (RAND_MAX / (hasta - desde + 1) + 1);
	return aleatorio;
}

// Funcion principal
void main(){
	// Declaracion de variables
	int ronda, apuestaAleatoria, apuesta, volado, eleccion, bolsaJugador, bolsaCPU, ganaRondaJugador, ganaRondaCPU;
	char mensajeError[]="La opcion debe de ser 1 o 2", gana[]="El jugador gano", pierde[]="La CPU gano", empate[]="Hubo un empate";
	
	bolsaJugador = bolsaCPU = 500;
		
	srand(getpid()); // Alimentar a la funcion rand con un valor no fijo (en este caso, el id del proceso del programa cambiara siempre) para generar n�meros aleatorios
	
	ronda = 1;
	
	puts("\t\t\tJUEGO DE VOLADOS");
		
	while (ronda <= rondas) {
		printf("\nRonda %d\n", ronda);
		apuestaAleatoria = random(apuestaMinima, apuestaMaxima); // Llamar a la funcion generadora de numeros aleatorios
		printf("Apuesta hecha por la computadora: $%d", apuestaAleatoria);
		printf("\nIntroduce tu apuesta entre %d y %d: $", apuestaMinima, apuestaMaxima);
		scanf("%d", &apuesta);
		
		if (apuesta >= apuestaMinima && apuesta <= apuestaMaxima) {
			puts("\nElige:");
			puts("1) AGUILA");
			puts("2) SOL");
			printf("Opcion: ");
			scanf("%d", &eleccion);
			if (eleccion == 1 || eleccion == 2) {
				volado = random(1, 2);
				if (volado == 1) {
					printf("\nEl resultado del volado fue AGUILA\n");
				} else {
					printf("\nEl resultado del volado fue SOL\n");
				}
				if (eleccion == volado) {
					printf("\nEl jugador gana esta ronda\n\n");
					ganaRondaJugador++;
					bolsaJugador += apuestaAleatoria;
					bolsaCPU -= apuestaAleatoria;
				} else {
					printf("\nLa CPU gana esta ronda\n\n");
					ganaRondaCPU++;
					bolsaJugador -= apuesta;
					bolsaCPU += apuesta;
				}
			} else {
				printf("\n%s\n", mensajeError);
			}	
		} else {
			printf("\nLa apuesta debe de estar entre $%d y $%d\n", apuestaMinima, apuestaMaxima);
		}
		ronda++;
	}
	
	// Imprimir en pantalla quien gano el juego
	if (ganaRondaJugador > ganaRondaCPU){
		printf("\n\t%s\n", gana);
	} else if (ganaRondaJugador < ganaRondaCPU) {
		printf("\n\t%s\n", pierde);
	} else {
		printf("\n\t%s\n", empate);
	}
	
	// Imprimir el monto de las bolsas
	printf("Dinero del jugador: $%d", bolsaJugador);
	printf("\nDinero de la CPU: $%d", bolsaCPU);
	
}
