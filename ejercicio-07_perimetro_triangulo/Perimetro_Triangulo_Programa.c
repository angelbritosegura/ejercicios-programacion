/* 
	Autor: Brito Segura Angel
	Fecha: 15/08/2020
	Descripcion: Programa que calcula el perimetro de un triangulo equilatero, isosceles o escaleno
*/

// Biblioteca estandar para entrada y salida
#include <stdio.h>
#include <ctype.h> // Para manejo de cadenas

// Funcion que despliega un peque�o menu
char menu() {
	char eleccion;
	puts("\n\t\t Calculo del perimetro de un triangulo:\n");
	puts("\ta) Equilatero");
	puts("\tb) Isosceles");
	puts("\tc) Escaleno");
	printf("\nSelecciona la letra acorde al triangulo que desea conocer su perimetro: ");
	fflush(stdin);
	scanf("%s", &eleccion);
	eleccion = tolower(eleccion);
	return eleccion;
}

// Funcion que calcula el perimetro de un triangulo equilatero
float equilatero() {
	float lado, operacion;
	
	printf("\nIntroduce un lado del triangulo equilatero: ");
	scanf("%f", &lado);
		
	operacion = 3*lado;
	
	return operacion;
}

// Funcion que calcula el perimetro de un triangulo isosceles
float isosceles() {
	float ladoRepetido, lado, operacion;
	
	printf("\nIntroduce el lado del triangulo isosceles que es igual a otro lado: ");
	scanf("%f", &ladoRepetido);
	printf("Introduce el otro lado faltante del triangulo isosceles: ");
	scanf("%f", &lado);
	
	operacion = 2*ladoRepetido + lado;
	
	return operacion;
}

// Funcion que calcula el perimetro de un triangulo escaleno
float escaleno() {
	float lado1, lado2, lado3, operacion;
	
	printf("\nIntroduce un lado del triangulo escaleno: ");
	scanf("%f", &lado1);
	printf("Introduce otro lado del triangulo escaleno: ");
	scanf("%f", &lado2);
	printf("Introduce el ultimo lado del triangulo escaleno: ");
	scanf("%f", &lado3);
	
	operacion = lado1 + lado2 + lado3;
	
	return operacion;
}

// Funcion que convierte a mayusculas una palabra ingresada
char convierteMayusculas () {
	// Declaracion de variables locales
	char palabra;
	
	printf("\nDesea obtener otro perimetro (SI/NO): ");
	fflush(stdin); // Limpieza de la variable de entrada para evitar errores
	scanf("%s", &palabra);
	palabra = toupper(palabra); // Convierte todo lo ingresado a mayusculas
	
	return (palabra); // Regresar el valor de la palabra ingresada
}

// Funcion principal
void main(){
	// Declaracion de variables locales
	char opcion, repetir;
	float perimetro;
	
	do {
		// Llamar al menu principal
		opcion = menu();
		
		// Dependiendo de la opcion sera el perimetro que se calculara
		switch(opcion) {
			case 'a':
				perimetro = equilatero();
				break;
			case 'b':
				perimetro = isosceles();
				break;
			case 'c':
				perimetro = escaleno();
				break;
			default:
				perimetro = -1;
				break;
		}
		
		// Si no existe la opcion ingresada
		if (perimetro == -1) {
			// Mostrara un mensaje de error
			printf("\nNo existe la opcion %c\n", opcion);
		} else {
			// En caso contrario, mostara el perimetro del traingulo introducido
			printf("\nEl perimetro del triangulo seleccionado es: %.1f\n", perimetro);
		}
		
		repetir = convierteMayusculas();
	} while(repetir == 'S'); // Mientras ingrese la palabra "SI" repetira el programa
}
